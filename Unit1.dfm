object Form1: TForm1
  Left = 254
  Top = 124
  Width = 390
  Height = 124
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 61
    Height = 13
    Caption = 'Printer name'
  end
  object Label2: TLabel
    Left = 8
    Top = 48
    Width = 45
    Height = 13
    Caption = 'File name'
  end
  object btnSave: TButton
    Left = 288
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 0
    OnClick = btnSaveClick
  end
  object edtPrinterName: TEdit
    Left = 88
    Top = 8
    Width = 177
    Height = 21
    TabOrder = 1
    Text = 'Foxit Reader PDF Printer'
  end
  object edtFileName: TEdit
    Left = 88
    Top = 48
    Width = 177
    Height = 21
    TabOrder = 2
    Text = 'foxit.b64'
  end
  object btnLoad: TButton
    Left = 288
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Load'
    TabOrder = 3
    OnClick = btnLoadClick
  end
  object enc64: TIdEncoderMIME
    FillChar = '='
    Left = 112
  end
  object dec64: TIdDecoderMIME
    FillChar = '='
    Left = 112
    Top = 40
  end
end

unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Printers,PrinterIO, IdBaseComponent, IdCoder,
  IdCoder3to4, IdCoderMIME,Math;

type
  TForm1 = class(TForm)
    btnSave: TButton;
    Label1: TLabel;
    edtPrinterName: TEdit;
    Label2: TLabel;
    edtFileName: TEdit;
    btnLoad: TButton;
    enc64: TIdEncoderMIME;
    dec64: TIdDecoderMIME;
    procedure btnSaveClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

const
  EOLN=#13#10;

implementation

{$R *.dfm}

procedure TForm1.btnSaveClick(Sender: TObject);
  var ms:TMemoryStream;
      ss,sl:TStringStream;
      fs:TFileStream;
      s:string;
begin
  ms:=TMemoryStream.Create;
  try
    s:=edtPrinterName.Text;
    SavePrinterInfo(PAnsiChar(s),ms);
    fs:=TFileStream.Create(edtFileName.Text,fmCreate);
    ms.Position:=0;

    ss:=TStringStream.Create(enc64.Encode(ms));
    s:=ss.ReadString(Min(80,ss.Size));
    while Length(s)>0 do
    begin
      sl:=TStringStream.Create(s+EOLN);
      fs.CopyFrom(sl,sl.Size);
      sl.Free;
      s:=ss.ReadString(Min(80,ss.Size));
    end;
    ss.Free;
    fs.Free;
  finally
    ms.Free;
  end;
end;

procedure TForm1.btnLoadClick(Sender: TObject);
  var ms:TMemoryStream;
      ss:TStringStream;
      fs:TFileStream;
      s:string;
begin
  fs:=TFileStream.Create(edtFileName.Text,fmOpenRead);
  ss:=TStringStream.Create('');
  ss.CopyFrom(fs,fs.Size);
  s:=ss.DataString;
  s:=StringReplace(s,EOLN,'',[rfReplaceAll]);
  ms:=TMemoryStream.Create;
  dec64.DecodeToStream(s,ms);
  ms.Position:=0;
  ss.Free;
  fs.Free;
  try
    s:=edtPrinterName.Text;
    LoadPrinterInfo(PAnsiChar(s),ms);
  finally
    ms.Free;
  end;

end;

end.
